import { Injectable } from '@angular/core';
import { Pet } from '../interfaces/pet';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PetService {

  constructor(private http: HttpClient) { }

  getPets(): Observable<Pet[]> {
    return this.http.get<Pet[]>('http://localhost:3000/pets');
  }

  getById(id: number): Observable<Pet> {
    return this.http.get<Pet>(`http://localhost:3000/pets/${id}`);
  }

  postNewPet(pet: Pet): Observable<Object> {
    return this.http.post('http://localhost:3000/pets', pet);
  }
}

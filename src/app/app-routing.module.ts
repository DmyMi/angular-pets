import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PetListComponent } from './pets/pet-list/pet-list.component';
import { PetDetailsComponent } from './pets/pet-details/pet-details.component';


const routes: Routes = [
  {path: 'list', component: PetListComponent},
  {path: 'pet/:id', component: PetDetailsComponent},
  {path: '', redirectTo: 'list', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

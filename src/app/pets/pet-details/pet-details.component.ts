import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Pet } from 'src/app/interfaces/pet';
import { PetService } from 'src/app/services/pet.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-pet-details',
  templateUrl: './pet-details.component.html',
  styleUrls: ['./pet-details.component.scss']
})
export class PetDetailsComponent implements OnInit, OnDestroy {

  pet: Pet;
  sub: Subscription;
  animalForm: FormGroup;
  newPet: Pet = {
    name: "",
    age: 0,
    type: ""
  }

  constructor(
    private petsSvc: PetService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit() {
    this.animalForm = new FormGroup({
      name: new FormControl(this.newPet.name, Validators.required),
      type: new FormControl(this.newPet.type, Validators.required),
      age: new FormControl(this.newPet.age, Validators.min(0))
    });
    this.sub = this.route.params
    .pipe(
      mergeMap((params, i) => {
        const id = Number.parseInt(params['id']);
        return this.petsSvc.getById(id);
      })
    ).subscribe(pet => {
        this.pet = pet;
      });
  }

  goBack() {
    this.router.navigate(['/list']);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  saveData() {
    console.log(JSON.stringify(this.pet));
  }

  saveReactive() {
    this.petsSvc.postNewPet(this.animalForm.value).subscribe(o => {
      console.log("success!");
    });
  }

}
